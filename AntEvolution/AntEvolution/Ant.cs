﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace AntEvolution
{
    public class Ant
    {

        private float locX;
        private float locY;
        private Vector2 location;
        
        private float maxSpeed;

        private float orientation;
        private Vector2 heading;

        private bool hasFood;
        private Vector2 foodLocation;
        private Vector2 nestLocation;

        private float sightRange;

        private Nest targetNest;
        private Food targetFood;

        protected Stopwatch foodMemSW = new Stopwatch();
        protected Stopwatch nestMemSW = new Stopwatch();
        protected Stopwatch wanderSW = new Stopwatch();
        float wanderTime = 500.0f;

        private float memoryLength;

        private Texture2D antTexture;

        private NeuralNet theBrain;
        private int lifeLength = 0; //this could be increased every time CheckNeuralInputs or maybe every time Live() is called?
        private double hunger;
        private double baseHunger = 1000;
        private double tiredness = 100;
        private Ant nearestAnt;
        private bool isAlive = true;
        private int health = 100;
        private bool doingAction = false;
        private AntEvent currentAction;


        protected int debug = 0;


        /// <summary>
        /// Default constructor
        /// </summary>
        public Ant()
        {
            LocX = 0.0f;
            LocY = 0.0f;
            MaxSpeed = 1.0f;
            HasFood = false;
            FoodLocation = new Vector2(-1, -1);
            NestLocation = new Vector2(-1, -1);
            SightRange = 30.0f;
            memoryLength = 3000.0f;
            Orientation = 1.0f;
            Heading = OrientationToVector(Orientation);
            hunger = baseHunger;
        }

        /// <summary>
        /// Create a new Ant object
        /// </summary>
        /// <param name="x">'x' component of location</param>
        /// <param name="y">'y' component of location</param>
        /// <param name="speed">maximum speed of the ant</param>
        /// <param name="holdingFood">is the ant holding food?</param>
        /// <param name="viewRange">distance other ants must be within to communicate</param>
        /// <param name="memLength">how long the ant can remember information for (ms)</param>
        public Ant(float x, float y, float speed, bool holdingFood, float viewRange, float memLength, Texture2D texture)
        {
            LocX = x;
            LocY = y;
            MaxSpeed = speed;
            HasFood = holdingFood;
            FoodLocation = new Vector2(-1, -1);
            NestLocation = new Vector2(-1, -1);
            SightRange = viewRange;
            memoryLength = memLength;
            Orientation = 1.0f;
            Heading = OrientationToVector(Orientation);
            AntTexture = texture;
            hunger = baseHunger;
        }

        /// <summary>
        /// Create a new Ant object
        /// </summary>
        /// <param name="x">'x' component of location</param>
        /// <param name="y">'y' component of location</param>
        /// <param name="speed">maximum speed of the ant</param>
        /// <param name="holdingFood">is the ant holding food?</param>
        /// <param name="viewRange">distance other ants must be within to communicate</param>
        /// <param name="memLength">how long the ant can remember information for (ms)</param>
        /// <param name="doDebug">do debug?</param>
        public Ant(float x, float y, float speed, bool holdingFood, float viewRange, float memLength, bool doDebug)
        {
            LocX = x;
            LocY = y;
            MaxSpeed = speed;
            HasFood = holdingFood;
            FoodLocation = new Vector2(-1, -1);
            NestLocation = new Vector2(-1, -1);
            SightRange = viewRange;
            memoryLength = memLength;
            Orientation = 1.0f;
            Heading = OrientationToVector(Orientation);
            if(doDebug){
                debug = 1;
            }
            hunger = baseHunger;
        }

        public NeuralNet TheBrain
        {
            get
            {
                return theBrain;
            }
            set
            {
                theBrain = value;
            }
        }

        public bool IsAlive
        {
            get
            {
                return isAlive;
            }
        }

        public Texture2D AntTexture
        {
            set
            {
                antTexture = value;
            }
            get
            {
                return antTexture;
            }
        }

        public Nest TargetNest
        {
            set
            {
                if (value != null)
                {
                    targetNest = value;
                    nestLocation = value.Location;
                }
            }
            get
            {
                return targetNest;
            }
        }

        public Food TargetFood
        {
            set
            {
                if (value != null)
                {
                    targetFood = value;
                    foodLocation = value.Location;
                }
            }
            get
            {
                return targetFood;
            }
        }

        public int LifeLength
        {
            get
            {
                return lifeLength;
            }
        }


        public float SightRange
        {
            protected set
            {
                if (value < 0)
                {
                    sightRange = 5.0f;
                }
                else
                {
                    sightRange = value;
                }
            }
            get
            {
                return sightRange;
            }
        }

        public float LocX
        {   
            set
            {
                if (value < 0)
                {
                    locX = 0.0f;
                    location.X = 0.0f;
                }
                else
                {
                    locX = value;
                    location.X = value;
                }
            }
            get
            {
                return locX;
            }
        }

        public float LocY
        {
            set
            {
                if (value < 0)
                {
                    locY = 0.0f;
                    location.Y = 0.0f;
                }
                else
                {
                    locY = value;
                    location.Y = value;
                }
            }
            get
            {
                return locY;
            }
        }

        public Vector2 Location
        {
            set
            {
                location = value;
                LocX = value.X;
                locY = value.Y;
            }
            get
            {
                return location;
            }
        }

        protected float MaxSpeed
        {
            set
            {
                maxSpeed = value;
            }
            get
            {
                return maxSpeed;
            }
        }

        public bool HasFood
        {
            set
            {
                hasFood = value;
            }
            get
            {
                return hasFood;
            }
        }

        private Vector2 Heading
        {
            set
            {
                heading = value;
            }
            get
            {
                return heading;
            }
        }

        public double Hunger
        {
            get
            {
                return hunger;
            }
        }

        public double Tiredness
        {
            get
            {
                return tiredness;
            }
        }

        public Ant NearestAnt
        {
            set
            {
                nearestAnt = value;
            }
        }

        public float Orientation
        {
            set
            {

                if (value < 0)
                {
                    orientation = value + MathHelper.ToRadians(360); 
                }
                if (value > MathHelper.ToRadians(360))
                {
                    orientation = value - MathHelper.ToRadians(360);
                }
                else
                {
                    orientation = value;
                }

                if (Heading != OrientationToVector(Orientation))
                {
                    Heading = OrientationToVector(Orientation); //update the heading information with the new orientation
                }
            }
            get
            {
                return orientation;
            }
        }

        public Vector2 FoodLocation
        {
            protected set
            {
                if (value.X < 0 || value.Y < 0)
                {
                    foodLocation = new Vector2(-1,-1);
                }
                else
                {
                    foodLocation = value;
                }
            }
            get
            {
                return foodLocation;
            }
        }

        public Vector2 NestLocation
        {
            protected set
            {
                if (value.X < 0 || value.Y < 0)
                {
                    nestLocation = new Vector2(-1, -1);
                }
                else
                {
                    nestLocation = value;
                }
            }
            get
            {
                return nestLocation;
            }
        }

        protected float MemoryLength
        {
            set
            {
                memoryLength = value;
            }
            get
            {
                return memoryLength;
            }
        }

        /// <summary>
        /// Move the ant randomly
        /// </summary>
        protected void Wander()
        {
            if (!wanderSW.IsRunning)
            {
                wanderSW.Start();
            }
                Random theRandom = new Random(Guid.NewGuid().GetHashCode()); //new random

                int degOrientation = (int)MathHelper.ToRadians(theRandom.Next(-360, 361));

                if (degOrientation - 20 < degOrientation + 21)
                {
                    Orientation += MathHelper.ToRadians((float)theRandom.Next(degOrientation - 20, degOrientation + 21)); //deviate from current heading by up to 20degs
                }
                else
                {
                    Orientation += MathHelper.ToRadians((float)theRandom.Next(degOrientation + 20, degOrientation - 21)); //deviate from current heading by up to 20degs
                }
                Heading = OrientationToVector(Orientation); //update heading

                Location += Heading * (MaxSpeed); //move
            
            hunger--;
        }

        /// <summary>
        /// Go towards a given location
        /// </summary>
        /// <param name="targetPos">Location to head towards</param>
        protected void GoTowards(Vector2 targetPos)
        {
            Heading = targetPos - Location; //calculate the direction as a vector

            Orientation = VectorToOrientation(Heading); //update orientation
            if (Heading != Vector2.Zero)
            {
                heading.Normalize();//normalise heading
            }

            Location += Heading * MaxSpeed; //move
        }

        /// <summary>
        /// Go towards a food object
        /// </summary>
        /// <param name="theFood">Food object to go towards</param>
        protected void GoTowards(Food theFood)
        {
            GoTowards(theFood.Location);
            hunger--;
        }

        /// <summary>
        /// Go towards a nest object
        /// </summary>
        /// <param name="theNest">nest to go towards</param>
        protected void GoTowards(Nest theNest)
        {
            GoTowards(theNest.Location);
            hunger--;
        }

        //public void GoTowards(Vector2 destination)
        //{
        //    Heading = destination - Location;//calculate the direction as vectio

        //    Orientation = VectorToOrientation(Heading);//update orientation
        //    if (Heading != Vector2.Zero)
        //    {
        //        heading.Normalize();//normalise heading
        //    }


        //    Location += Heading * MaxSpeed; //move
        //}

        private bool CheckAtDestination(Vector2 destination)
        {
            if (DistanceTo(destination) < 1.0f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// convert an orientation in radians to a heading
        /// </summary>
        /// <param name="deg">orientation to convert</param>
        /// <returns>Heading</returns>
        private Vector2 OrientationToVector(float deg)
        {
            Vector2 direction = new Vector2();

            direction.X = (float)Math.Cos(deg); //Cos of orientation get us our horizontal
            direction.Y = (float)Math.Sin(deg); //Sin gets us our vertical

            return direction;
        }
        
        /// <summary>
        /// Convert a vector heading to an orientation
        /// </summary>
        /// <param name="direction">heading vector</param>
        /// <returns>orientation</returns>
        private float VectorToOrientation(Vector2 direction)
        {
            float deg;

            if (direction != Vector2.Zero)
            {
                deg = (float)Math.Atan2(direction.Y, direction.X); //inverse tan of (Y,X) gives us the orientation
            }
            else
            {
                deg = 0;
            }
            return deg;
        }

        private void DoDecision(AntEvent choice)
        {

            switch (choice)
            {
                case AntEvent.ATTACK: //do nothing
                    Attack();
                    break;
                case AntEvent.MOVE_RAND:
                    doingAction = true;
                    currentAction = AntEvent.MOVE_RAND;
                    Wander();
                    break;
                case AntEvent.PICKUP_FOOD:
                    PickUpFood();
                    break;
                case AntEvent.EAT_FOOD:
                    Eat();
                    break;
                case AntEvent.DROP_FOOD:
                    DropFood();
                    break;
                case AntEvent.GOTO_NEST:
                    doingAction = true;
                    currentAction = AntEvent.GOTO_NEST;

                    if (nestLocation != new Vector2(-1, -1))
                    {
                        GoTowards(nestLocation);
                    }
                    else
                    {
                        doingAction = false;
                        hunger++;
                    }
                    break;
                case AntEvent.GOTO_FOOD:
                    doingAction = true;
                    currentAction = AntEvent.GOTO_FOOD;

                    if (foodLocation != new Vector2(-1, -1))
                    {
                        GoTowards(foodLocation);
                    }
                    else
                    {
                        doingAction = false;
                        hunger++;
                    }
                    break;

            }

            //if (choice != 0) //if we do anything other than nothoing at all...
            //{
            //    hunger--; //get a bit more hungry
            //    tiredness--; //and a bit more tired
            //}
        }

        private void CheckActionsComplete()
        {
            switch (currentAction)
            {
                case AntEvent.MOVE_RAND:
                    if (wanderSW.IsRunning)
                    {
                        if (wanderSW.ElapsedMilliseconds > wanderTime)
                        {
                            wanderSW.Stop();
                            wanderSW.Reset();
                            doingAction = false;
                        }
                    }
                    break;
                case AntEvent.GOTO_FOOD:
                    if (CheckAtDestination(foodLocation))
                    {
                        doingAction = false;
                    }
                    break;
                case AntEvent.GOTO_NEST:
                    if (CheckAtDestination(nestLocation))
                    {
                        doingAction = false;
                    }
                    break;
            }
        }

        private void PickUpFood()
        {
            if (DistanceTo(targetFood.Location) < sightRange)
            {
                if (targetFood.TakeFood())
                {
                    hasFood = true;
                }
            }
            hunger--;
        }

        private void DropFood()
        {
            if (hasFood)
            {
                if (DistanceTo(NestLocation) < sightRange)
                {
                    targetNest.Food++;
                    hasFood = false;
                }
                else
                {
                    hasFood = false;
                }
            }
            hunger--;
        }

        private void Attack()
        {
            if (nearestAnt != null)
            {
                if (DistanceTo(nearestAnt.location) < 1.0)
                {
                    nearestAnt.TakeDamage(10);
                    if (hunger + 10 < baseHunger)
                    {
                        hunger += 10;
                    }
                    else
                    {
                        hunger = baseHunger;
                    }
                }
            }
        }

        public void TakeDamage(int amount)
        {
            health -= amount;
        }

        private void Rest()
        {
            tiredness++;
        }

        private void Eat()
        {
            if (HasFood)
            {
                hunger = baseHunger;
                hasFood = false;
            }
            else if (TargetNest != null && DistanceTo(NestLocation) < sightRange && targetNest.Food > 0)
            {
                targetNest.Food--;
                hunger = baseHunger;
            }
            else if (TargetFood != null && DistanceTo(foodLocation) < sightRange && targetFood.FoodPieces > 0)
            {
                targetFood.FoodPieces--;
                hunger = baseHunger;
            }
            else
            {
                hunger--;
            }
        }

        private void MakeDecision()
        {
            if (!doingAction)
            {
                DoDecision(theBrain.ChooseOutput());
            }
            else
            {
                DoDecision(currentAction);
                CheckActionsComplete();
            }
        }

        private float DistanceTo(Vector2 otherObject)
        {
            Vector2 difference = Location - otherObject;
            float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

            return distance;
        }

        /// <summary>
        /// Ant 'brain'. If called every tick the ant will perform its behaviour.
        /// </summary>
        public void Live()
        {
            if (health < 0)
            {
                isAlive = false;
            }

            if (isAlive) //if we're alive do all the things that are genome-defined
            {
                MakeDecision();
                lifeLength++;


                //Anything past here is considered instinctual

                if (hunger < 10)
                {
                    TakeDamage(1);
                }


                Vector2 minusOneVect = new Vector2(-1, -1);

                if (foodMemSW.ElapsedMilliseconds > memoryLength) //if we have known food location for longer than our memory length...
                {
                    ForgetFoodLocation(); //forget food location
                }
                if (nestMemSW.ElapsedMilliseconds > memoryLength)//if we have known nest location for longer than our memory length...
                {
                    ForgetNestLocation(); //forget nest location
                }

            }

            if (debug > 0) // if debug flag is set...
            {
                Debug.WriteLine("Foodmem: {0}, Nestmem: {1}", foodMemSW.ElapsedMilliseconds, nestMemSW.ElapsedMilliseconds); //show the degbug info
            }
        }

        /// <summary>
        /// Learn the location of a Food object
        /// </summary>
        /// <param name="theFood">food object to learn location of</param>
        public void LearnFoodLocation(Food theFood)
        {
            foodMemSW = Stopwatch.StartNew(); //start food memory timer
            FoodLocation = theFood.Location; //set the food location
            TargetFood = theFood; //set a reference to the food
        }

        /// <summary>
        /// Learn the location of a Nest object
        /// </summary>
        /// <param name="theNest">nest object to learn the location of</param>
        public void LearnNestLocation(Nest theNest)
        {
            nestMemSW = Stopwatch.StartNew(); //start nest memory timer
            NestLocation = theNest.Location; //learn nest location
            TargetNest = theNest; //set reference to the nest
        }

        /// <summary>
        /// Forget the location of the Food object
        /// </summary>
        public void ForgetFoodLocation()
        {
            foodMemSW.Reset(); //reset the timer to 0
            FoodLocation = new Vector2(-1, -1); //set flag so we know we don't have a food location
            TargetFood = null; //lose the reference to the food
        }

        /// <summary>
        /// Forget the location of the Nest object
        /// </summary>
        public void ForgetNestLocation()
        {
            nestMemSW.Reset(); //reset timer to very
            NestLocation = new Vector2(-1, -1); //set flag so we know we don't have a nest location
            TargetNest = null; //lose refernce to the nest
        }

        /// <summary>
        /// Rotate the ant
        /// </summary>
        /// <param name="amount">amount to rotate by</param>
        public void Rotate(float amount)
        {
            Orientation += amount;
            Heading = OrientationToVector(Orientation); //update heading to match
        }

        public void CopyValues(Ant defaultAnt)
        {
            hasFood = defaultAnt.HasFood;
            lifeLength = defaultAnt.LifeLength;
            nestLocation = defaultAnt.NestLocation;
            foodLocation = defaultAnt.FoodLocation;
            theBrain = defaultAnt.TheBrain;
            hunger = defaultAnt.Hunger;
            isAlive = defaultAnt.isAlive;
            health = defaultAnt.health;
            Debug.WriteLine("" + isAlive + "vs " + defaultAnt.isAlive);
        }
    }
}
