﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AntEvolution
{
    public enum AntEvent
    {
        MEET_ANT,
        MEET_FOOD,
        MEET_NEST,
        ATTACK,
        EAT_FOOD,
        PICKUP_FOOD,
        DROP_FOOD,
        MOVE_RAND,
        GOTO_NEST,
        GOTO_FOOD
    }
}
