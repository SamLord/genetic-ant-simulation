using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace AntEvolution
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class AntProj : Microsoft.Xna.Framework.Game
    {
        //declare/initalise global variable
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private Texture2D workerAntTexture;
        private List<WorkerAnt> workerAnts = new List<WorkerAnt>();
        private List<PredatorAnt> predatorAnts = new List<PredatorAnt>();
        private List<Nest> nests = new List<Nest>();
        private List<Food> foodPiles = new List<Food>();
        private List<Nest> predatorAntNests = new List<Nest>();
        Rectangle antRectangle;
        Texture2D nestTexture;
        Texture2D predatorNestTexture;
        Texture2D foodTexture;
        Texture2D workerAntFoodTexture;
        Texture2D predatorAntTexture;
        Texture2D predatorAntFoodTexture;
        Color background = Color.LightGoldenrodYellow;
        SpriteFont textFont;
        Vector2 fontPos;
        int origBackBufferHeight;
        int origBackBufferWidth;

        public AntProj()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            origBackBufferHeight = graphics.PreferredBackBufferHeight;
            origBackBufferWidth = graphics.PreferredBackBufferWidth;
            Content.RootDirectory = "Content"; //set root directory for our assets
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();

            DistributeWorkerAnts(200); //create all the worker ants
            DistributePredatorAnts(140); //...and all the predator ants
            IsMouseVisible = true;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            
            //load all the textures
            workerAntTexture = Content.Load<Texture2D>("sprites\\ant");
            nestTexture = Content.Load<Texture2D>("sprites\\nest");
            predatorNestTexture = Content.Load<Texture2D>("sprites\\nestRed");
            foodTexture = Content.Load<Texture2D>("sprites\\food");
            workerAntFoodTexture = Content.Load<Texture2D>("sprites\\antFood");
            predatorAntTexture = Content.Load<Texture2D>("sprites\\RedAnt");
            predatorAntFoodTexture = Content.Load<Texture2D>("sprites\\RedAntFood");

            //load font and set its position
            textFont = Content.Load<SpriteFont>("fonts\\textFont");
            fontPos = new Vector2(5, 5);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            // TODO: Add your update logic here
            CheckKeyPress(); //check for key/mouse presses and react to user input
            base.Update(gameTime);

            MoveAnts(); //make the ants do something

            RemoveEmptyFood(); //get rid of any food that has been emptied

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(background);


            spriteBatch.Begin();
            //for each of the type of objects (predator ants, worker ants, nests and food)...
            if (workerAnts.Count != 0)
            {
                Vector2 origin = new Vector2(workerAnts.ElementAt(0).AntTexture.Width / 2, workerAnts.ElementAt(0).AntTexture.Height / 2); //find the middle of the texture

                foreach (WorkerAnt theAnt in workerAnts)
                {
                    Rectangle tempRect = new Rectangle((int)theAnt.LocX, (int)theAnt.LocY, 10, 10); //draw a rectangle around the object
                    spriteBatch.Draw(theAnt.AntTexture, tempRect, null, background, theAnt.Orientation+MathHelper.ToRadians(90), origin, SpriteEffects.None, 1.0f); //draw it to screen
                }
            }
            if (predatorAnts.Count != 0)
            {
                Vector2 origin = new Vector2(workerAnts.ElementAt(0).AntTexture.Width / 2, workerAnts.ElementAt(0).AntTexture.Height / 2);

                foreach (PredatorAnt theAnt in predatorAnts)
                {
                    Rectangle tempRect = new Rectangle((int)theAnt.LocX, (int)theAnt.LocY, 10, 10);
                    spriteBatch.Draw(theAnt.AntTexture, tempRect, null, background, theAnt.Orientation + MathHelper.ToRadians(90), origin, SpriteEffects.None, 1.0f);
                }
            }
            if (nests.Count != 0)
            {
                Vector2 origin = new Vector2(nests.ElementAt(0).NestTexture.Width / 2, nests.ElementAt(0).NestTexture.Height / 2);

                foreach (Nest theNest in nests)
                {
                    Rectangle tempRect = new Rectangle((int)theNest.LocX, (int)theNest.LocY, (int)(9 * theNest.Scale), (int)(9 * theNest.Scale));
                    spriteBatch.Draw(theNest.NestTexture, tempRect, null, background, 0.0f, origin, SpriteEffects.None, 1.0f);
                }
            }
            if (predatorAntNests.Count != 0)
            {
                Vector2 origin = new Vector2(predatorAntNests.ElementAt(0).NestTexture.Width / 2, predatorAntNests.ElementAt(0).NestTexture.Height / 2);

                foreach (Nest theNest in predatorAntNests)
                {
                    Rectangle tempRect = new Rectangle((int)theNest.LocX, (int)theNest.LocY, (int)(9 * theNest.Scale), (int)(9 * theNest.Scale));
                    spriteBatch.Draw(theNest.NestTexture, tempRect, null, background, 0.0f, origin, SpriteEffects.None, 1.0f);
                }
            }
            if (foodPiles.Count != 0)
            {
                Vector2 origin = new Vector2(foodPiles.ElementAt(0).FoodTexture.Width / 2, foodPiles.ElementAt(0).FoodTexture.Height / 2);

                foreach (Food theFood in foodPiles)
                {
                    Rectangle tempRect = new Rectangle((int)theFood.LocX, (int)theFood.LocY, (int)(theFood.Scale), (int)(theFood.Scale));
                    spriteBatch.Draw(theFood.FoodTexture, tempRect, null, background, 0.0f, origin, SpriteEffects.None, 1.0f);
                }
            }
            spriteBatch.DrawString(textFont, "Red: " + predatorAnts.Count().ToString() + "\nBlack: " + workerAnts.Count().ToString() + "\nFood: " + GetFoodAmount().ToString(), fontPos, Color.DarkSlateGray); //draw out gui to screen
            
            spriteBatch.End(); //finish the sprite batch

            base.Draw(gameTime);
        }

        /// <summary>
        /// Checks for user input and performs actions as appropriate
        /// </summary>
        private void CheckKeyPress()
        {
            //mouse inputs
            MouseState mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                nests.Add(new Nest(mouseState.X, mouseState.Y, 1.0f, nestTexture));//create nest at mouse location
            }
            if (mouseState.RightButton == ButtonState.Pressed)
            {
                foodPiles.Add(new Food(mouseState.X, mouseState.Y, 150, foodTexture)); //food at mouse location
            }

            //keyboard inputs
            KeyboardState keyState = Keyboard.GetState();

            if (keyState.IsKeyDown(Keys.Escape))
            {
                Exit();//quit the 'game'
            }
            if (keyState.IsKeyDown(Keys.N))
            {
                predatorAntNests.Add(new Nest(mouseState.X, mouseState.Y, 1.0f, predatorNestTexture)); //predator ants nest at mouse location
            }
            //when delete key is pressed remove all stationary objects from the screen
            //and make all the ants forget the old locations
            if (keyState.IsKeyDown(Keys.Delete))
            {
                //clear the lists to remove objects
                nests.Clear(); 
                foodPiles.Clear();
                predatorAntNests.Clear();
                foreach (WorkerAnt theAnt in workerAnts) //for each work ant...
                {
                    //call the forget methods
                    theAnt.ForgetFoodLocation();
                    theAnt.ForgetNestLocation();
                }
                foreach (PredatorAnt theAnt in predatorAnts)//for each predator ant...
                {
                    //call the forget methods
                    theAnt.ForgetFoodLocation();
                    theAnt.ForgetNestLocation();
                }
            }
            if(keyState.IsKeyDown(Keys.Enter)){
                //clear and redistribute ants
                workerAnts.Clear();
                predatorAnts.Clear();
                DistributeWorkerAnts(200);
                DistributePredatorAnts(250);
            }
            if (keyState.IsKeyDown(Keys.F))
            {
                if (graphics.IsFullScreen == false)
                {
                    //set new backBuffer
                    graphics.PreferredBackBufferWidth = 1280;
                    graphics.PreferredBackBufferHeight = 720;
                    //apply the change
                    graphics.ApplyChanges();
                    //go fullscreen
                    graphics.ToggleFullScreen();
                }
                else
                {
                    //set back buffer to what it was at launch
                    graphics.PreferredBackBufferWidth = origBackBufferWidth;
                    graphics.PreferredBackBufferHeight = origBackBufferHeight;
                    //apply
                    graphics.ApplyChanges();
                    //leave full screen
                    graphics.ToggleFullScreen();
                }
            }
        }

        /// <summary>
        /// Adds up the total food in all the food pile
        /// </summary>
        /// <returns>total amount of food in the food piles</returns>
        private int GetFoodAmount()
        {
            int amount=0;

            if (foodPiles.Count != 0) //if there are food piles to check
            {
                foreach (Food theFood in foodPiles) //for every food pile
                {
                    amount += theFood.FoodPieces; //total up the number of food pieces
                }
            }

            return amount;
        }

        /// <summary>
        /// Gets a random between 2 numbers
        /// </summary>
        /// <param name="min">lower bound</param>
        /// <param name="max">upper bound</param>
        /// <returns>random number between min and max</returns>
        private float GetRandomBetween(int min, int max)
        {
            float tempRandom = 0.0f;

            //create a 'Random' object and seed it with
            //"Guid.NewGuid().GetHashCode()" to avoid duplicate seeds and therefore
            //duplicate random numbers causing unrealistic behaviour
            Random rand = new Random(Guid.NewGuid().GetHashCode()); 

            while (tempRandom == 0.0f) //we don't want 0
            {
                tempRandom = rand.Next(min, max + 1); //get the next random number
            }
            
            return tempRandom;
        }

        /// <summary>
        /// Creates x number of worker ants with a random location and orientation
        /// </summary>
        /// <param name="numberOfAnts"></param>
        private void DistributeWorkerAnts(int numberOfAnts)
        {
            int screenWidth = GraphicsDevice.Viewport.Width;
            int screenHeight = GraphicsDevice.Viewport.Height;

            for (int i = 0; i < numberOfAnts; i++)
            {
                antRectangle.X = (int)GetRandomBetween(1, screenWidth - 2); //only put ants between x=1 and x= screen width (i.e inside the window) (-2 to decrease chances of getting stuck)
                antRectangle.Y = (int)GetRandomBetween(1, screenHeight - 2); // the same for y axis
                workerAnts.Add(new WorkerAnt(antRectangle.X, antRectangle.Y, MathHelper.ToRadians(GetRandomBetween(0, 360)), 0.9f, false, workerAntTexture)); //place an ant at the location we just created
            }
        }

        /// <summary>
        /// Creates x number of predator ants with a random location and orientation
        /// </summary>
        /// <param name="numberOfAnts"></param>
        private void DistributePredatorAnts(int numberOfAnts)
        {
            int screenWidth = GraphicsDevice.Viewport.Width;
            int screenHeight = GraphicsDevice.Viewport.Height;

            for (int i = 0; i < numberOfAnts; i++)
            {
                antRectangle.X = (int)GetRandomBetween(1, screenWidth - 2);//only put ants between x=1 and x= screen width (i.e inside the window) (-2 to decrease chances of getting stuck)
                antRectangle.Y = (int)GetRandomBetween(1, screenHeight - 2);// the same for y axis
                predatorAnts.Add(new PredatorAnt(antRectangle.X, antRectangle.Y, (float)MathHelper.ToRadians(GetRandomBetween(0,361)), 1.0f, false, predatorAntTexture, 3000.0f));//place an ant at the location we just created
            }
        }

        /// <summary>
        /// if the given ant is outside the screen it is rotated 180 degrees
        /// </summary>
        /// <param name="theAnt"></param>
        private void KeepAntsOnScreen(Ant theAnt)
        {
            if ((int)theAnt.LocX + theAnt.AntTexture.Width > GraphicsDevice.Viewport.Width || theAnt.LocX <= 0) //the ant is outside the sides of the screen
            {
                theAnt.Rotate(MathHelper.ToRadians(180)); //turn it around (this looks more natural than 'bouncing' off the wall
            }
            //same for y axis (top and bottom of the screen
            if ((int)theAnt.LocY + theAnt.AntTexture.Height > GraphicsDevice.Viewport.Height || theAnt.LocY <= 0)
            {
                theAnt.Rotate(MathHelper.ToRadians(180));
            }
        }

        /// <summary>
        /// Ant logic - learning food/nest locations etc 
        /// </summary>
        private void MoveAnts()
        {
            foreach (WorkerAnt theAnt in workerAnts)
            {
                KeepAntsOnScreen(theAnt); //make sure the ant stays inside the window
                foreach (Food theFood in foodPiles) //for each foodpile
                {
                    Vector2 difference = theAnt.Location - theFood.Location; //calculate how far away we are as vector
                    float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y)); //change the vector into a float distance
                    if (distance < theAnt.SightRange && !theAnt.HasFood) //if the ant can see it
                    {
                        theAnt.LearnFoodLocation(theFood);//learn where it is
                    }
                    if (distance < 1.0f && theFood.FoodPieces > 0) //if it is very close
                    {
                        theAnt.HasFood = true; //collect food
                        theFood.TakeFood(); //deduct what we've taken from the food pile
                        theAnt.AntTexture = workerAntFoodTexture; //change the texture to one where the ant is carrying food
                    }
                }
                foreach (Nest theNest in nests)
                {
                    //as above calculate distance from the nest
                    Vector2 difference = theAnt.Location - theNest.Location;
                    float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

                    if (distance < theAnt.SightRange)//if the ant can see it
                    {
                        theAnt.LearnNestLocation(theNest); //learn its location
                    }
                    if (distance < 1.0f) //if we're really close
                    {
                        theAnt.HasFood = false; //deliver any food we're carrying
                        theAnt.AntTexture = workerAntTexture; //set texture to one without food
                    }
                }
                foreach (WorkerAnt otherAnt in workerAnts)
                {
                    LearnInfo(otherAnt, theAnt);//learn information from nearby ants (distance calc done inside the method)
                }

                theAnt.Live();//run the method that acts as the ant's brain
            }
            foreach (PredatorAnt theAnt in predatorAnts)
            {
                KeepAntsOnScreen(theAnt);
                foreach (WorkerAnt workerAnt in workerAnts)
                {
                    Vector2 difference = theAnt.Location - workerAnt.Location;
                    float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

                    if (distance < theAnt.SightRange && workerAnt.HasFood)
                    {
                        theAnt.TargetFood = workerAnt;
                    }
                    if (distance < 0.5f && workerAnt.HasFood)
                    {
                        theAnt.TakeFood(workerAnt, workerAntTexture,predatorAntFoodTexture);
                    }
                } 
                foreach (Nest theNest in predatorAntNests)
                {
                    Vector2 difference = theAnt.Location - theNest.Location;
                    float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

                    if (distance < theAnt.SightRange)
                    {
                        theAnt.LearnNestLocation(theNest);
                    }
                    if (distance < 1.0f)
                    {
                        theAnt.HasFood = false;
                        theAnt.AntTexture = predatorAntTexture;
                    }
                }
                foreach (PredatorAnt otherAnt in predatorAnts)
                {
                    LearnInfo(otherAnt, theAnt); //learn locations of food and nest from nearby ants (check for distance done within the method)
                }

                theAnt.Live();//run brain code
            }
        }

        /// <summary>
        /// removes empty food piles in the foodPiles list
        /// </summary>
        private void RemoveEmptyFood()
        {
            for (int i = foodPiles.Count() - 1; i >= 0; i--)
            {
                if ( foodPiles[i].FoodPieces < 1) //when piles have run out of food
                {
                    foodPiles.Remove(foodPiles[i]); //remove them
                }
            }
        }

        /// <summary>
        /// Teaches theAnt otherAnt's knowledge
        /// </summary>
        /// <param name="otherAnt">teacher</param>
        /// <param name="theAnt">student</param>
        private void LearnInfo(Ant otherAnt, Ant theAnt)
        {
            //calculate the distance between the 2 ants
            Vector2 difference = theAnt.Location - otherAnt.Location; 
            float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

            if (distance < theAnt.SightRange / 2) //if they can see each other
            {
                if (theAnt.NestLocation == new Vector2(-1, -1) && otherAnt.NestLocation != new Vector2(-1, -1)) //if the teacher ant knows a nest location the student ant doesn't
                {
                    if (otherAnt.TargetNest != null) //and the teacher has a reference to the nest
                    {
                        theAnt.LearnNestLocation(otherAnt.TargetNest); //student ant learns teacher ant's knowledge
                    }
                }
                if (theAnt.FoodLocation == new Vector2(-1, -1) && otherAnt.FoodLocation != new Vector2(-1, -1)) //if the teacher knows a food location and we don't already have one
                {
                    if (otherAnt.TargetFood != null) // and the teacher has a reference to the food
                    {
                        theAnt.LearnFoodLocation(otherAnt.TargetFood); //student learns teacher's knowledge
                    }
                }
            }
        }

        /// <summary>
        /// Was used to show debug info for the given ants, now just displays "Debug info"
        /// </summary>
        /// <param name="antOne"></param>
        /// <param name="antTwo"></param>
        /// <param name="antThree"></param>
        private void DebugAnts(int antOne, int antTwo, int antThree) //shown in class diagrams and so left here for clarity but now unused
        {
            Debug.WriteLine("[Debug info]");
        }

    }
}
