﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AntEvolution
{
    [Serializable()]
    public class BaseNodeConnection : ISerializable
    {
        private InputNode input;
        private float weighting;

        public BaseNodeConnection(SerializationInfo info, StreamingContext context)
        {
            input = (InputNode)info.GetValue("input", typeof(InputNode));
            weighting = (float)info.GetValue("weighting", typeof(float));
        }

        public BaseNodeConnection(InputNode inNode, float weight)
        {
            input = inNode;
            weighting = weight;
        }

        public InputNode Input
        {
            get
            {
                return input;
            }
        }

        public float Weighting
        {
            get
            {
                return weighting;
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("input", input);
            info.AddValue("weighting", weighting);
        }
    }
}
