﻿namespace AntEvolution
{
    partial class InfoPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfoPanel));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveBestNeuralNetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSimulationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartWithLoadedBrainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveBrainDialogue = new System.Windows.Forms.SaveFileDialog();
            this.openBrainDialog = new System.Windows.Forms.OpenFileDialog();
            this.doDrawCheckBox = new System.Windows.Forms.CheckBox();
            this.algorithmSelectionBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.controlsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(377, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveBestNeuralNetToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveBestNeuralNetToolStripMenuItem
            // 
            this.saveBestNeuralNetToolStripMenuItem.Name = "saveBestNeuralNetToolStripMenuItem";
            this.saveBestNeuralNetToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveBestNeuralNetToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.saveBestNeuralNetToolStripMenuItem.Text = "Save Best Neural Net...";
            this.saveBestNeuralNetToolStripMenuItem.Click += new System.EventHandler(this.saveBestNeuralNetToolStripMenuItem_Click);
            // 
            // controlsToolStripMenuItem
            // 
            this.controlsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetSimulationToolStripMenuItem,
            this.restartWithLoadedBrainToolStripMenuItem});
            this.controlsToolStripMenuItem.Name = "controlsToolStripMenuItem";
            this.controlsToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.controlsToolStripMenuItem.Text = "Controls";
            // 
            // resetSimulationToolStripMenuItem
            // 
            this.resetSimulationToolStripMenuItem.Name = "resetSimulationToolStripMenuItem";
            this.resetSimulationToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.resetSimulationToolStripMenuItem.Text = "Reset Simulation";
            this.resetSimulationToolStripMenuItem.Click += new System.EventHandler(this.resetSimulationToolStripMenuItem_Click);
            // 
            // restartWithLoadedBrainToolStripMenuItem
            // 
            this.restartWithLoadedBrainToolStripMenuItem.Name = "restartWithLoadedBrainToolStripMenuItem";
            this.restartWithLoadedBrainToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.restartWithLoadedBrainToolStripMenuItem.Text = "Restart with loaded brain";
            this.restartWithLoadedBrainToolStripMenuItem.Click += new System.EventHandler(this.restartWithLoadedBrainToolStripMenuItem_Click);
            // 
            // openBrainDialog
            // 
            this.openBrainDialog.FileName = "openFileDialog1";
            // 
            // doDrawCheckBox
            // 
            this.doDrawCheckBox.AutoSize = true;
            this.doDrawCheckBox.Checked = true;
            this.doDrawCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.doDrawCheckBox.Location = new System.Drawing.Point(12, 141);
            this.doDrawCheckBox.Name = "doDrawCheckBox";
            this.doDrawCheckBox.Size = new System.Drawing.Size(106, 17);
            this.doDrawCheckBox.TabIndex = 3;
            this.doDrawCheckBox.Text = "View Simulation?";
            this.doDrawCheckBox.UseVisualStyleBackColor = true;
            this.doDrawCheckBox.CheckedChanged += new System.EventHandler(this.doDrawCheckBox_CheckedChanged);
            // 
            // algorithmSelectionBox
            // 
            this.algorithmSelectionBox.FormattingEnabled = true;
            this.algorithmSelectionBox.Items.AddRange(new object[] {
            "Simple",
            "Original",
            "Even-Originial",
            "Random",
            "Amalgamation"});
            this.algorithmSelectionBox.Location = new System.Drawing.Point(12, 27);
            this.algorithmSelectionBox.Name = "algorithmSelectionBox";
            this.algorithmSelectionBox.Size = new System.Drawing.Size(106, 95);
            this.algorithmSelectionBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(346, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "When restarting the simulation the above selected algorithm will be used";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // versionToolStripMenuItem
            // 
            this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
            this.versionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.versionToolStripMenuItem.Text = "Version...";
            this.versionToolStripMenuItem.Click += new System.EventHandler(this.versionToolStripMenuItem_Click);
            // 
            // InfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 165);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.algorithmSelectionBox);
            this.Controls.Add(this.doDrawCheckBox);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "InfoPanel";
            this.Text = "Control Panel";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveBestNeuralNetToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveBrainDialogue;
        private System.Windows.Forms.OpenFileDialog openBrainDialog;
        private System.Windows.Forms.CheckBox doDrawCheckBox;
        private System.Windows.Forms.ToolStripMenuItem controlsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetSimulationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartWithLoadedBrainToolStripMenuItem;
        private System.Windows.Forms.ListBox algorithmSelectionBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
    }
}