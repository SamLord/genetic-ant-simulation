﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace AntEvolution
{
    public partial class InfoPanel : Form
    {
        private Simulation parentSim;
        private string version = "1.0.0.4"; //System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); 

        public InfoPanel(Simulation parent)
        {
            InitializeComponent();
            parentSim = parent;
        }

        
        private void saveBestNeuralNetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            
            saveBrainDialogue.ShowDialog();
            SaveBestBrain(saveBrainDialogue.FileName);
            
        }

        private void doDrawCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            parentSim.DoDraw(doDrawCheckBox.Checked);
        }

        private void resetSimulationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure? Brain will not be saved.", "Reset Sim?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                parentSim.ResetSimulation(parentSim.CreateNeuralNet());
            }
        }

        public NeuralNet LoadBrain()
        {
            openBrainDialog.ShowDialog();
            NeuralNet theBrain;
            try
            {
                IFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(openBrainDialog.FileName, FileMode.Open);
                theBrain = (NeuralNet)formatter.Deserialize(stream);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to load brain: " + ex.Message);
                theBrain = null;
            }

            return theBrain;
        }

        public void SaveBestBrain(string filename)
        {
            NeuralNet bestBrain = parentSim.GetMostSuccessfulAnt().TheBrain;

            try
            {
                IFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(filename, FileMode.Create);
                formatter.Serialize(stream, bestBrain);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to save Brain: " + ex.Message);
            }

        }

        private void restartWithLoadedBrainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure? Current brain will not be saved.", "Restart with loaded brain?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                try
                {
                    NeuralNet loadedBrain = LoadBrain();
                    if (loadedBrain != null)
                    {
                        parentSim.ResetSimulation(loadedBrain);
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Failed to restart simulation with loaded brain: " + ex.Message);
                }
            }
        }

        private void versionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Current version: " + version);
        }
    }
}
