﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AntEvolution
{
    [Serializable()]
    public class InputNode : ISerializable
    {
        private List<NodeConnection> connections = new List<NodeConnection>();
        private AntEvent theEvent;

        public InputNode(SerializationInfo info, StreamingContext context)
        {
            connections = (List<NodeConnection>)info.GetValue("connections", typeof(List<NodeConnection>));
            theEvent = (AntEvent)info.GetValue("theEvent", typeof(AntEvent));
        }

        public InputNode(AntEvent triggerEvent)
        {
            theEvent = triggerEvent;
        }

        public List<OutputNode> getOutputNodes()
        {
            List<OutputNode> outNodes = new List<OutputNode>();

            foreach (NodeConnection connection in connections)
            {
                outNodes.Add(connection.Output);
            }

            return outNodes;
        }

        public List<NodeConnection> getNodeConnections()
        {
            return connections;
        }

        public void addNewOutputNode(OutputNode outNode, float weight)
        {
            connections.Add(new NodeConnection(outNode,weight));
        }



        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("connections", connections);
            info.AddValue("theEvent", theEvent);
        }
    }
}
