﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AntEvolution
{
    [Serializable()]
    public class NeuralNet : ISerializable
    {
        private List<InputNode> inNodes;
        private List<OutputNode> outNodes;
        private RootNode root = new RootNode();

        public NeuralNet(SerializationInfo info, StreamingContext ctxt)
        {
            inNodes = (List<InputNode>)info.GetValue("inNodes", typeof(List<InputNode>));
            outNodes = (List<OutputNode>)info.GetValue("outNodes", typeof(List<OutputNode>));
            root = (RootNode)info.GetValue("root", typeof(RootNode));
        }

        public NeuralNet(List<InputNode> inputNodes, List<OutputNode> outputNodes)
        {
            inNodes = inputNodes;
            outNodes = outputNodes;

            float chanceVal = 100/inputNodes.Count;
            foreach (InputNode inNode in inNodes)
            {
                root.addNewInputNode(inNode, chanceVal);
            }
        }

        public bool CreateConnection(InputNode inNode, OutputNode outNode, float weight)
        {
            bool added = false;
            InputNode theInput = new InputNode(AntEvent.MEET_ANT);

            foreach (InputNode thisNode in inNodes)
            {
                if (thisNode == inNode)
                {
                    theInput = thisNode;
                    theInput.addNewOutputNode(outNode, weight);
                    added = true;
                    break;
                }
            }

            return added;
        }

        public AntEvent ChooseOutput()
        {
            float sum = 0;

            foreach (BaseNodeConnection connection in root.Inputs)
            {
                sum += connection.Weighting;
            }

            int randSeed = 0;
            randSeed += Guid.NewGuid().GetHashCode();
            randSeed *= this.GetHashCode();
            randSeed *= (int)sum;

            int choice = new Random(randSeed).Next((int)sum);

            float count = 0.0f;
            int index = 0;
            for (int i = 0; i < root.Inputs.Count; i++)
            {
                count += root.Inputs.ElementAt(i).Weighting;

                if (count > choice)
                {
                    index = i;
                    break;
                }
            }

            InputNode givenInput = root.Inputs.ElementAt(index).Input;

            sum = 0;
            foreach (NodeConnection connection in givenInput.getNodeConnections())
            {
                sum += connection.Weighting;
            }

            choice = new Random().Next((int)sum);

            count = 0.0f;
            index = 0;
            for (int i = 0; i < givenInput.getNodeConnections().Count; i++)
            {
                count += givenInput.getNodeConnections().ElementAt(i).Weighting;

                if (count > choice)
                {
                    index = i;
                    break;
                }
            }

            AntEvent action = givenInput.getNodeConnections().ElementAt(index).Output.ActionToTake;

            return action;

        }

        public NeuralNet GetNeuralNetCopy()
        {
            NeuralNet newNet = new NeuralNet(inNodes, outNodes);
            
            return newNet;
        }

        
        public NeuralNet GetMutatedNeuralNet()
        {
            NeuralNet newNet = this;

            foreach (InputNode theNode in newNet.inNodes)
            {
                Random rand = new Random(Guid.NewGuid().GetHashCode());
                foreach (NodeConnection connection in theNode.getNodeConnections())
                {

                    double variation = rand.NextDouble();

                    while (variation < 0.1)
                    {
                        variation = rand.NextDouble();
                    }

                    int posNegDecider = rand.Next(0, 101);
                    float change = connection.Weighting / (float)variation;

                    while (change > (connection.Weighting / 10))
                    {
                        change = change / (float)variation * 0.1f;
                    }

                    if (posNegDecider >= 50)
                    {
                        connection.Weighting += change;
                    }
                    else
                    {
                        connection.Weighting -= change;
                    }
                }
            }

            return newNet;
        }

        public NeuralNet GetEvenMutatedNeuralNet(float maxChange)
        {
            NeuralNet newNet = this;

            foreach (InputNode theNode in newNet.inNodes)
            {
                float downChangeMax = maxChange;
                float upChangeMax = maxChange;

                Random rand = new Random(Guid.NewGuid().GetHashCode());
                foreach (NodeConnection connection in theNode.getNodeConnections())
                {

                    double variation;

                    do
                    {
                        variation = rand.NextDouble();
                    }while (variation < 0.1);
                    

                    int posNegDecider = rand.Next(0, 101);

                    float change;

                    if (posNegDecider >= 50)
                    {
                        change = GetChangeValue(connection, variation, upChangeMax);

                        connection.Weighting += change;
                        upChangeMax -= change;
                        downChangeMax += change;
                    }
                    else
                    {
                        change = GetChangeValue(connection, variation, downChangeMax);

                        connection.Weighting -= change;
                        upChangeMax += change;
                        downChangeMax -= change;
                    }
                }
            }

            return newNet;
        }

        private float GetChangeValue(NodeConnection connection, double variation, float maxVal)
        {
            int attempt = 0;
            bool attemptMaxReached = false;
            float change;
            
                do
                {
                    attempt++;
                    change = connection.Weighting / (float)variation;

                    while (change > (connection.Weighting / 10))
                    {
                        change = change / (float)variation * 0.1f;
                    }

                    if (attempt > 50)
                    {
                        attemptMaxReached = true;
                        break;
                    }

                } while (change >= maxVal);

                if (attemptMaxReached)
                {
                    Random rand = new Random(Guid.NewGuid().GetHashCode());

                    change = (float)rand.NextDouble();
                }

            return change;
        }

        public NeuralNet GetSimpleMutatedNeuralNet()
        {
            NeuralNet newNet = this;

            foreach (InputNode theNode in newNet.inNodes)
            {
                Random rand = new Random(Guid.NewGuid().GetHashCode());
                foreach (NodeConnection connection in theNode.getNodeConnections())
                {

                    int posNegDecider = rand.Next(0, 101);
                    float change = (float)rand.NextDouble() * 10;

                    if (posNegDecider >= 50)
                    {
                        connection.Weighting += change;
                    }
                    else
                    {
                        connection.Weighting -= change;
                    }
                }
            }

            return newNet;
        }

        public NeuralNet GetRandomMutatedNeuralNet()
        {
            NeuralNet newNet = this;

            foreach (InputNode theNode in newNet.inNodes)
            {
                int seed = Guid.NewGuid().GetHashCode() * this.GetHashCode() * root.GetHashCode() * DateTime.Now.Millisecond;
                Random rand = new Random(seed);

                foreach (NodeConnection connection in theNode.getNodeConnections())
                {
                    double rand1 = rand.NextDouble();
                    int rand2 = rand.Next();
                    int rand3 = rand.Next();
                    double rand4 = (rand2 + rand3) * rand1 * rand1;
                    
                    float change;
                    if (((rand2 - rand3) * rand4) > 0)
                    {
                        change = (float)Math.Log10((rand2 - rand3) * rand4);
                    }
                    else
                    {
                        change = (float)Math.Log10(-(rand2 - rand3) * rand4);
                        change -= (change * 2);
                    }

                    connection.Weighting += change;
                }
            }

            return newNet;
        }

        public NeuralNet GetAmalgamationMutatedNeuralNet()
        {
            NeuralNet newNet = this;

            newNet = GetRandomMutatedNeuralNet();
            newNet = GetSimpleMutatedNeuralNet();
            newNet = GetEvenMutatedNeuralNet(newNet.GetHashCode());
            newNet = GetMutatedNeuralNet();

            return newNet;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("inNodes", inNodes);
            info.AddValue("outNodes", outNodes);
            info.AddValue("root", root);
        }
    }
    
}
