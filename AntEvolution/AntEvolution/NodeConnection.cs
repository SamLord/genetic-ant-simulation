﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AntEvolution
{
    [Serializable()]
    public class NodeConnection : ISerializable
    {
        private OutputNode output;
        private float weighting;

        public NodeConnection(SerializationInfo info, StreamingContext context)
        {
            output = (OutputNode)info.GetValue("output", typeof(OutputNode));
            weighting = (float)info.GetValue("weighting", typeof(float));
        }

        public NodeConnection(OutputNode outNode, float weight)
        {
            output = outNode;
            weighting = weight;
        }

        public OutputNode Output
        {
            get
            {
                return output;
            }
        }

        public float Weighting
        {
            get
            {
                return weighting;
            }
            set
            {
                if (value > 0)
                {
                    weighting = value;
                }
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("output", output);
            info.AddValue("weighting", weighting);
        }
    }
}
