﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AntEvolution
{
    [Serializable()]
    public class OutputNode : ISerializable
    {
        //action to take
        AntEvent actionToTake;

        public OutputNode(SerializationInfo info, StreamingContext context)
        {
            actionToTake = (AntEvent)info.GetValue("actionToTake", typeof(AntEvent));
        }

        public OutputNode(AntEvent theEvent)
        {
            actionToTake = theEvent;
        }

        public AntEvent ActionToTake
        {
            get
            {
                return actionToTake;
            }
        }


        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("actionToTake", actionToTake);
        }
    }
}
