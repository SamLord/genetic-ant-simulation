using System;

namespace AntEvolution
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            using (Simulation game = new Simulation())
            {
                game.Run();
            }
        }
    }
#endif
}

