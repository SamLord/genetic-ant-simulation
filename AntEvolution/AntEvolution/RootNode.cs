﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AntEvolution
{
    [Serializable()]
    public class RootNode : ISerializable
    {
        private List<BaseNodeConnection> inputs = new List<BaseNodeConnection>();

        public RootNode(SerializationInfo info, StreamingContext context)
        {
            inputs = (List<BaseNodeConnection>)info.GetValue("inputs", typeof(List<BaseNodeConnection>));
        }

        public RootNode()
        {
        }

        public List<BaseNodeConnection> Inputs
        {
            get
            {
                return inputs;
            }
        }

        public void addNewInputNode(InputNode inNode, float weight)
        {
            inputs.Add(new BaseNodeConnection(inNode, weight));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("inputs", inputs);
        }
    }
}
