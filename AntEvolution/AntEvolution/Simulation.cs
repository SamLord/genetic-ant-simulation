using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace AntEvolution
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Simulation : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        int origBackBufferHeight;
        int origBackBufferWidth;

        int numberOfAnts = 250;
        int deadAntCount = 0;

        private Ant defaultAnt;
        private List<Ant> ants = new List<Ant>();
        private List<Ant> deadAnts = new List<Ant>();
        private List<Nest> nests = new List<Nest>();
        private List<Food> foodPiles = new List<Food>();
        private Texture2D antTexture;
        private Texture2D antWithFoodTexture;
        private Texture2D bestAntTexture;
        private Texture2D foodTexture;
        private Texture2D nestTexture;
        private Texture2D deadAntTexture;
        private Rectangle antRectangle;
        private SpriteFont textFont;
        private Vector2 fontPos;

        private int generationCount = 0;
        private int thisGenerationTick = 0;
        private int highestTick = 0;
        private int lastTick = 0;
        private float variation = 10.0f;
        Color background = Color.SandyBrown;

        private InfoPanel infoForm;
        private Ant bestAntYet = new Ant();

        bool doDraw = true;

        public Simulation()
        {
            infoForm = new InfoPanel(this);
            infoForm.Show();
            graphics = new GraphicsDeviceManager(this);
            this.IsMouseVisible = true;

            origBackBufferHeight = graphics.PreferredBackBufferHeight;
            origBackBufferWidth = graphics.PreferredBackBufferWidth;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {

            NeuralNet newBrain = CreateNeuralNet();

            // TODO: Add your initialization logic here
            ResetSimulation(newBrain);

            base.Initialize();
        }

        public void ResetSimulation(NeuralNet baseBrain)
        {
            generationCount = 0;
            thisGenerationTick = 0;
            highestTick = 0;
            lastTick = 0;

            ants.Clear();
            foodPiles.Clear();
            nests.Clear();

            antTexture = Content.Load<Texture2D>("Sprites\\ant");
            nestTexture = Content.Load<Texture2D>("Sprites\\nest");
            foodTexture = Content.Load<Texture2D>("Sprites\\food");
            deadAntTexture = Content.Load<Texture2D>("Sprites\\deadAnt");
            antWithFoodTexture = Content.Load<Texture2D>("Sprites\\antFood");
            bestAntTexture = Content.Load<Texture2D>("Sprites\\bestAnt");


            textFont = Content.Load<SpriteFont>("fonts\\textFont");
            fontPos = new Vector2(5, 5);

            foodPiles.Add(new Food(100, 250, 150, foodTexture));
            foodPiles.Add(new Food(700, 300, 150, foodTexture));

            nests.Add(new Nest(100, 350, 1.0f, nestTexture));
            nests.Add(new Nest(300, 100, 1.0f, nestTexture));
            nests.Add(new Nest(500, 125, 1.0f, nestTexture));

            GenerateAnts(numberOfAnts);


            foreach (Ant theAnt in ants)
            {
                theAnt.TheBrain = baseBrain.GetNeuralNetCopy();
            }

            Food exFood = new Food();

            foreach (Food thisPile in foodPiles)
            {
                thisPile.FoodPieces = exFood.FoodPieces;
            }

            foreach (Nest thisNest in nests)
            {
                thisNest.Food = 0;
            }
        }

        public void DoDraw(bool draw)
        {
            doDraw = draw;
        }

        public Ant GetMostSuccessfulAnt()
        {
            Ant bestAnt = null;
            int biggestLifeTime = 0;

            foreach (Ant thisAnt in ants)
            {
                if (thisAnt.LifeLength > biggestLifeTime)
                {
                    biggestLifeTime = thisAnt.LifeLength;
                    bestAnt = thisAnt;
                }
            }

            return bestAnt;
        }

        public NeuralNet CreateNeuralNet()
        {
            List<InputNode> inNodes = new List<InputNode>();
            List<OutputNode> outNodes = new List<OutputNode>();
            InputNode tempInput;

            OutputNode attack = new OutputNode(AntEvent.ATTACK);
            OutputNode eat = new OutputNode(AntEvent.EAT_FOOD);
            OutputNode pickFood = new OutputNode(AntEvent.PICKUP_FOOD);
            OutputNode dropFood = new OutputNode(AntEvent.DROP_FOOD);
            OutputNode moveRand = new OutputNode(AntEvent.MOVE_RAND);
            OutputNode gotoFood = new OutputNode(AntEvent.GOTO_FOOD);
            OutputNode gotoNest = new OutputNode(AntEvent.GOTO_NEST);

            outNodes.Add(attack);
            outNodes.Add(eat);
            outNodes.Add(pickFood);
            outNodes.Add(dropFood);
            outNodes.Add(moveRand);
            outNodes.Add(gotoFood);
            outNodes.Add(gotoNest);

            //Alternate Starting NN
            //tempInput = new InputNode(AntEvent.MEET_ANT);
            //tempInput.addNewOutputNode(attack, 33.3f);
            //tempInput.addNewOutputNode(dropFood, 33.3f);
            //tempInput.addNewOutputNode(moveRand, 33.3f);
            //inNodes.Add(tempInput);

            //tempInput = new InputNode(AntEvent.MEET_FOOD);
            //tempInput.addNewOutputNode(eat, 25.0f);
            //tempInput.addNewOutputNode(dropFood, 25.0f);
            //tempInput.addNewOutputNode(moveRand, 25.0f);
            //tempInput.addNewOutputNode(gotoNest, 25.0f);
            //inNodes.Add(tempInput);

            //tempInput = new InputNode(AntEvent.MEET_NEST);
            //tempInput.addNewOutputNode(dropFood, 25.0f);
            //tempInput.addNewOutputNode(eat, 25.0f);
            //tempInput.addNewOutputNode(moveRand, 25.0f);
            //tempInput.addNewOutputNode(gotoFood, 25.0f);
            //inNodes.Add(tempInput);


            tempInput = new InputNode(AntEvent.MEET_ANT);
            tempInput.addNewOutputNode(attack, 200.0f);
            tempInput.addNewOutputNode(dropFood, 200.0f);
            tempInput.addNewOutputNode(moveRand, 200.0f);
            inNodes.Add(tempInput);

            tempInput = new InputNode(AntEvent.MEET_FOOD);
            tempInput.addNewOutputNode(eat, 200.0f);
            tempInput.addNewOutputNode(dropFood, 200.0f);
            tempInput.addNewOutputNode(moveRand, 200.0f);
            tempInput.addNewOutputNode(gotoNest, 200.0f);
            inNodes.Add(tempInput);

            tempInput = new InputNode(AntEvent.MEET_NEST);
            tempInput.addNewOutputNode(dropFood, 200.0f);
            tempInput.addNewOutputNode(eat, 200.0f);
            tempInput.addNewOutputNode(moveRand, 200.0f);
            tempInput.addNewOutputNode(gotoFood, 200.0f);
            inNodes.Add(tempInput);



            NeuralNet newBrain = new NeuralNet(inNodes, outNodes);
            return newBrain;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit


            CheckKeyPress();


            bool continueSimulation = SimulateAnts();

            if (!continueSimulation)
            {
                NewGeneration(GetMostSuccessfulAnt());
                generationCount++;
                if (thisGenerationTick > highestTick)
                {
                    highestTick = thisGenerationTick;
                }
                lastTick = thisGenerationTick;
                thisGenerationTick = 0;
            }





            base.Update(gameTime);
        }

        private void NewGeneration(Ant baseAnt)
        {
            if (baseAnt.LifeLength > bestAntYet.LifeLength)
            {
                bestAntYet = baseAnt;
            }
            NeuralNet bestBrain = baseAnt.TheBrain;

            infoForm.SaveBestBrain("Brain_Backup.dat");

            foreach (Ant thisAnt in ants)
            {
                thisAnt.CopyValues(defaultAnt);
                thisAnt.AntTexture = antTexture;
                //thisAnt.TheBrain = bestBrain.GetEvenMutatedNeuralNet(variation);
                //thisAnt.TheBrain = bestBrain.GetMutatedNeuralNet();
                //thisAnt.TheBrain = bestBrain.GetSimpleMutatedNeuralNet();
                //thisAnt.TheBrain = bestBrain.GetRandomMutatedNeuralNet();
                thisAnt.TheBrain = bestBrain.GetAmalgamationMutatedNeuralNet();
            }

            baseAnt.CopyValues(defaultAnt);
            baseAnt.TheBrain = bestBrain;
            baseAnt.AntTexture = bestAntTexture;
            ants[0] = baseAnt;
            DistributeAnts(ants);

            deadAntCount = 0;
            deadAnts = new List<Ant>();
        }

        /// <summary>
        /// Runs the simulation returning a bool of true if there is at least 1 ant alive
        /// </summary>
        /// <returns></returns>
        private bool SimulateAnts()
        {
            thisGenerationTick++;
            bool oneAlive = false;

            foreach (Ant theAnt in ants)
            {
                if (theAnt.IsAlive)
                {
                    if (theAnt.HasFood)
                    {
                        theAnt.AntTexture = antWithFoodTexture;
                    }

                    oneAlive = true;
                    theAnt.Live();

                    LearnInfoBySelf(theAnt);

                    float minDistance = 1000.0f;
                    Ant nearestAnt = new Ant();
                    foreach (Ant otherAnt in ants)
                    {
                      if(otherAnt != theAnt)
                      {
                          float separation = GetDistanceBetween(otherAnt.Location, theAnt.Location);
                          if (separation < minDistance)
                          {
                              minDistance = separation;
                              nearestAnt = otherAnt;
                          }
                          LearnInfoFromOther(otherAnt, theAnt); //this could have an 'if' to check the separation and save checking distances twice?
                        }
                    }
                    theAnt.NearestAnt = nearestAnt;

                    KeepAntOnScreen(theAnt);

                }
                else
                {
                    if (theAnt.AntTexture == antTexture)
                    {
                        theAnt.AntTexture = deadAntTexture;
                    }
                    if (deadAnts.IndexOf(theAnt) < 0)
                    {
                        deadAnts.Add(theAnt);
                        deadAntCount++;
                    }
                }
            }

            return oneAlive;
        }

        private void LearnInfoFromOther(Ant otherAnt, Ant theAnt)
        {
            //calculate the distance between the 2 ants
            Vector2 difference = theAnt.Location - otherAnt.Location;
            float distance = GetDistanceBetween(theAnt.Location, otherAnt.Location);

            if (distance < theAnt.SightRange / 2) //if they can see each other
            {
                if (theAnt.NestLocation == new Vector2(-1, -1) && otherAnt.NestLocation != new Vector2(-1, -1)) //if the teacher ant knows a nest location the student ant doesn't
                {
                    if (otherAnt.TargetNest != null) //and the teacher has a reference to the nest
                    {
                        theAnt.LearnNestLocation(otherAnt.TargetNest); //student ant learns teacher ant's knowledge
                    }
                }
                if (theAnt.FoodLocation == new Vector2(-1, -1) && otherAnt.FoodLocation != new Vector2(-1, -1)) //if the teacher knows a food location and we don't already have one
                {
                    if (otherAnt.TargetFood != null) // and the teacher has a reference to the food
                    {
                        theAnt.LearnFoodLocation(otherAnt.TargetFood); //student learns teacher's knowledge
                    }
                }
            }
        }

        private void LearnInfoBySelf(Ant theAnt)
        {
            foreach (Food theFood in foodPiles)
            {
                if (GetDistanceBetween(theAnt.Location, theFood.Location) < theAnt.SightRange)
                {
                    theAnt.LearnFoodLocation(theFood);
                    break;
                }
            }

            foreach (Nest theNest in nests)
            {
                if (GetDistanceBetween(theAnt.Location, theNest.Location) < theAnt.SightRange)
                {
                    theAnt.LearnNestLocation(theNest);
                    break;
                }
            }
        }

        private float GetDistanceBetween(Vector2 firstPos, Vector2 secondPos)
        {
            Vector2 difference = firstPos - secondPos;
            float distance = (float)Math.Sqrt((difference.X * difference.X) + (difference.Y * difference.Y));

            return distance;
        }

        private void KeepAntOnScreen(Ant theAnt)
        {
            if ((int)theAnt.LocX + theAnt.AntTexture.Width > GraphicsDevice.Viewport.Width || theAnt.LocX <= 0) //the ant is outside the sides of the screen
            {
                theAnt.Rotate(MathHelper.ToRadians(180)); //turn it around (this looks more natural than 'bouncing' off the wall
            }
            //same for y axis (top and bottom of the screen
            if ((int)theAnt.LocY + theAnt.AntTexture.Height > GraphicsDevice.Viewport.Height || theAnt.LocY <= 0)
            {
                theAnt.Rotate(MathHelper.ToRadians(180));
                theAnt.TakeDamage(1);
            }
        }

        private void CheckKeyPress()
        {
            //keyboard inputs
            KeyboardState keyState = Keyboard.GetState();

            if (keyState.IsKeyDown(Keys.F))
            {
                if (graphics.IsFullScreen == false)
                {
                    //set new backBuffer
                    graphics.PreferredBackBufferWidth = 1280;
                    graphics.PreferredBackBufferHeight = 720;
                    //apply the change
                    graphics.ApplyChanges();
                    //go fullscreen
                    graphics.ToggleFullScreen();
                }
                else
                {
                    //set back buffer to what it was at launch
                    graphics.PreferredBackBufferWidth = origBackBufferWidth;
                    graphics.PreferredBackBufferHeight = origBackBufferHeight;
                    //apply
                    graphics.ApplyChanges();
                    //leave full screen
                    graphics.ToggleFullScreen();
                }
            }
        }

        private void GenerateAnts(int numberOfAnts)
        {
            int screenWidth = GraphicsDevice.Viewport.Width;
            int screenHeight = GraphicsDevice.Viewport.Height;

            for (int i = 0; i < numberOfAnts; i++)
            {
                antRectangle.X = (int)GetRandomBetween(1, screenWidth - 2); //only put ants between x=1 and x= screen width (i.e inside the window) (-2 to decrease chances of getting stuck)
                antRectangle.Y = (int)GetRandomBetween(1, screenHeight - 2); // the same for y axis
                ants.Add(new Ant(antRectangle.X,antRectangle.Y,0.9f,false,5.0f,5.0f,antTexture)); //place an ant at the location we just created
                //new Ant(antRectangle.X, antRectangle.Y, MathHelper.ToRadians(GetRandomBetween(0, 360)), 0.9f, false, antTexture)
            }

            defaultAnt = new Ant(antRectangle.X, antRectangle.Y, 0.9f, false, 5.0f, 5.0f, antTexture);
        }

        private void DistributeAnts(List<Ant> antList)
        {
            int screenWidth = GraphicsDevice.Viewport.Width;
            int screenHeight = GraphicsDevice.Viewport.Height;

            foreach (Ant currentAnt in ants)
            {
                antRectangle.X = (int)GetRandomBetween(1, screenWidth - 2); //only put ants between x=1 and x= screen width (i.e inside the window) (-2 to decrease chances of getting stuck)
                antRectangle.Y = (int)GetRandomBetween(1, screenHeight - 2); // the same for y axis
                currentAnt.Location = new Vector2(antRectangle.X, antRectangle.Y);
            }
        }

        private float GetRandomBetween(int min, int max)
        {
            float tempRandom = 0.0f;

            //create a 'Random' object and seed it with
            //"Guid.NewGuid().GetHashCode()" to avoid duplicate seeds and therefore
            //duplicate random numbers causing unrealistic behaviour
            Random rand = new Random(Guid.NewGuid().GetHashCode());

            while (tempRandom == 0.0f) //we don't want 0
            {
                tempRandom = rand.Next(min, max + 1); //get the next random number
            }

            return tempRandom;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(background);


            spriteBatch.Begin();

            spriteBatch.DrawString(textFont, "Generation: " + generationCount + Environment.NewLine + "Tick: " + thisGenerationTick + Environment.NewLine + "Highest Tick: " + highestTick + Environment.NewLine + "Last Tick: " + lastTick + Environment.NewLine + "Alive: " + (numberOfAnts-deadAntCount) + Environment.NewLine + "Dead: " + deadAntCount, fontPos, Color.DarkSlateGray); //draw out gui to screen

            if (doDraw)
            {
                //for each of the type of objects (predator ants, worker ants, nests and food)...
                if (ants.Count != 0)
                {
                    Vector2 origin = new Vector2(ants.ElementAt(0).AntTexture.Width / 2, ants.ElementAt(0).AntTexture.Height / 2); //find the middle of the texture

                    foreach (Ant theAnt in ants)
                    {
                        Rectangle tempRect = new Rectangle((int)theAnt.LocX, (int)theAnt.LocY, 10, 10); //draw a rectangle around the object
                        spriteBatch.Draw(theAnt.AntTexture, tempRect, null, background, theAnt.Orientation + MathHelper.ToRadians(90), origin, SpriteEffects.None, 1.0f); //draw it to screen
                    }
                }
                if (nests.Count != 0)
                {
                    Vector2 origin = new Vector2(nests.ElementAt(0).NestTexture.Width / 2, nests.ElementAt(0).NestTexture.Height / 2);

                    foreach (Nest theNest in nests)
                    {
                        Rectangle tempRect = new Rectangle((int)theNest.LocX, (int)theNest.LocY, (int)(9 * theNest.Scale), (int)(9 * theNest.Scale));
                        spriteBatch.Draw(theNest.NestTexture, tempRect, null, background, 0.0f, origin, SpriteEffects.None, 1.0f);
                    }
                }
                if (foodPiles.Count != 0)
                {
                    Vector2 origin = new Vector2(foodPiles.ElementAt(0).FoodTexture.Width / 2, foodPiles.ElementAt(0).FoodTexture.Height / 2);

                    foreach (Food theFood in foodPiles)
                    {
                        Rectangle tempRect = new Rectangle((int)theFood.LocX, (int)theFood.LocY, (int)(theFood.Scale), (int)(theFood.Scale));
                        spriteBatch.Draw(theFood.FoodTexture, tempRect, null, background, 0.0f, origin, SpriteEffects.None, 1.0f);
                    }
                }
            }

            spriteBatch.End(); //finish the sprite batch

            base.Draw(gameTime);
        }
    }
}
