﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace AntEvolution
{
    public class StationaryGameObj
    {

        private float locX;
        private float locY;
        private Vector2 location;
        private float scale;
        private Rectangle boundingBox;

        /// <summary>
        /// Default constructor
        /// </summary>
        public StationaryGameObj()
        {
            LocX = 0;
            LocY = 0;
        }

        /// <summary>
        /// Create a StationaryGameObj
        /// </summary>
        /// <param name="X">'x' component of location</param>
        /// <param name="Y">'y' component of location</param>
        /// <param name="size">scale multiplier</param>
        /// <param name="bounds">bounds of the object</param>
        public StationaryGameObj(float X, float Y, float size, Rectangle bounds)
        {
            LocX = X;
            locY = Y;
            Scale = size;
            BoundingBox = bounds;
        }

        /// <summary>
        /// Create a StationaryGameObj
        /// </summary>
        /// <param name="loc">object's location</param>
        public StationaryGameObj(Vector2 loc)
        {
            Location = loc;
        }

        public Rectangle BoundingBox
        {
            set
            {
                boundingBox = value;
            }
        }

        public Vector2 Location
        {
            set
            {
                location = value;
                LocX = value.X;
                locY = value.Y;
            }
            get
            {
                return location;
            }
        }

        public float LocX
        {
            set
            {
                if (value < 0)
                {
                    locX = 0.0f;
                    location.X = 0.0f;
                }
                else
                {
                    locX = value;
                    location.X = value;
                }
            }
            get
            {
                return locX;
            }
        }

        public float LocY
        {
            set
            {
                if (value < 0)
                {
                    locY = 0.0f;
                    location.Y = 0.0f;
                }
                else
                {
                    locY = value;
                    location.Y = value;
                }
            }
            get
            {
                return locY;
            }
        }

        public float Scale
        {
            set
            {
                if (value <= 0)
                {
                    scale = 1;
                }
                else
                {
                    scale = value;
                }
            }
            get
            {
                return scale;
            }
        }
    }
}
